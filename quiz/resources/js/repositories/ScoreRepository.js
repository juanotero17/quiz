import Repository from 'axios';

const resource = '/users';

export default {
    get() {
     return Repository.get(`${resource}`);
    },

     post() {
         return Repository.post(`${resource}`);
        }
}